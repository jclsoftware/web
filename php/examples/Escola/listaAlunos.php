<!DOCTYPE html>
<html>
<head>
	<title>Trabalho Prático 2</title>
	<meta charset="utf-8">
</head>
<body>

	 
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "turma";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT * FROM Aluno";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "Matricula: " . $row["Matricula"]. " - Nome: " . $row["Nome"]. " " . $row["Idade"]. " " . $row["CPF"]. "<br>";
    }
} else {
    echo "0 results";
}
$conn->close();
?>
</body>
</html>