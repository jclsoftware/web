CREATE TABLE `aluno` (
  `Matricula` int(11) NOT NULL,
  `Nome` varchar(100) NOT NULL,
  `Idade` int(11) NOT NULL,
  `CPF` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `aluno` (`Matricula`, `Nome`, `Idade`, `CPF`) VALUES
(1, 'Jean Carlo', 27, 635020606),
(2, 'Jean', 27, 635020606),
(3, 'JeanCarlo', 27, 635020606),
(4, 'Jean Carlos', 27, 635020606),
(5, 'JeanCarlos', 27, 635020606),
(6, 'Gean', 27, 635020606),
(7, 'GeanCarlo', 27, 635020606),
(8, 'GeanCarlos', 27, 635020606),
(9, 'Gean Carlo', 27, 635020606),
(14, 'Anitta', 35, 123456789);

ALTER TABLE `aluno`
  ADD PRIMARY KEY (`Matricula`),
  ADD UNIQUE KEY `CPF` (`CPF`);
COMMIT;