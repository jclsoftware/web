<?php
require_once "config.php";
$query_select = "SELECT Id, Username FROM User";

if ($result = mysqli_query($connect, $query_select)) {
  while ($row = mysqli_fetch_row($result)) {
    printf ("<br>%s (%s)\n", $row[0], $row[1]);
  }
  mysqli_free_result($result);
}

mysqli_close($connect);